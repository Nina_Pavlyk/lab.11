import pandas as pd

# Завантаження даних з файлу data.csv
file_path = 'data.csv'
try:
    data = pd.read_csv(file_path, on_bad_lines='skip')
except pd.errors.ParserError as e:
    print(f"Помилка при читанні файлу: {e}")
    data = pd.DataFrame()  # Створюємо порожній DataFrame

# Виведення вмісту .csv файлу на екран
print("Зміст файлу:")
print(data)

# Перевірка, чи є дані для подальших дій
if not data.empty:
    # Організація пошуку даних для введених користувачем назв країн
    countries_to_search = input("Введіть назви країн (через кому): ").split(', ')

    # Виконання пошуку в DataFrame
    search_results = data[data['Country Name'].isin(countries_to_search)]

    # Виведення результатів пошуку на екран
    print("\nРезультати пошуку:")
    print(search_results)

    # Запис результатів пошуку у новий .csv файл
    output_file_path = 'search_results.csv'
    search_results.to_csv(output_file_path, index=False)
    print(f"\nРезультати пошуку записано у файл: {output_file_path}")
else:
    print("Немає даних для пошуку через помилку у вихідному файлі.")
